FROM openjdk:8-jdk-alpine

WORKDIR /app

COPY target/JpwCreditCard-0.0.1-SNAPSHOT.jar /app/jpw-service-card.jar

ENTRYPOINT ["java", "-jar", "jpw-service-card.jar"]