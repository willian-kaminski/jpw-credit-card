package com.br.jpw.JpwCreditCard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpwCreditCardApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpwCreditCardApplication.class, args);
	}

}
