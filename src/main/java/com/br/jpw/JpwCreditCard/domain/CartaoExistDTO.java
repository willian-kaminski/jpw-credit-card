package com.br.jpw.JpwCreditCard.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartaoExistDTO {

    private String cpf;
    private Boolean existe;

}
