package com.br.jpw.JpwCreditCard.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@Entity
public class Cartao {

    private static final long serialVersionUID = -1905907502453138175L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "Identificador interno do Cartão")
    private Long id;

    @NotNull
    @ApiModelProperty(value = "Proprietario do cartão")
    private Cliente cliente;

    @Size(max = 20)
    @ApiModelProperty(value = "Código do Cartão")
    private String codigoCartao;

    @NotNull
    @ApiModelProperty(value = "Categoria do Cartão")
    private Categoria categoria;

    @NotNull()
    @ApiModelProperty(value = "Limite do Cartão")
    private Double limite;

    @ApiModelProperty(value = "Data de validade do cartão")
    private Date dataValidade;

    @ApiModelProperty(value = "Atributo para identificar se o cartão ainda esta ativo")
    private boolean isAtivo;

}
