package com.br.jpw.JpwCreditCard.util;

import com.br.jpw.JpwCreditCard.domain.Categoria;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Component
public class RegrasCartao {

    private Integer ANO_PARA_VENCIMENTO = 5;
    private Double LIMITE_GOLD = 100000D;
    private Double LIMITE_BLACK = 0D;
    private Double LIMITE_DIAMANTE = 500000D;

    public String gerarCodigoCartao(){

        String codigo = "";
        Random random = new Random();

        for(int i = 0; i < 20; i++){
            codigo += random.nextInt(10);
        }

        return codigo;
    }

    public Date gerarDataValidade(){
        Calendar calendar = new Calendar.Builder().build();
        calendar.setTime(new Date());
        calendar.add(Calendar.YEAR, ANO_PARA_VENCIMENTO);
        return calendar.getTime();
    }

    public Categoria gerarCategoria(Double valorRenda){
        if(valorRenda <= 10000){
            return Categoria.GOLD;
        }else if(valorRenda <= 50000){
            return Categoria.DIAMANTE;
        }
        return Categoria.BLACK;
    }

    public Double gerarLimite(Categoria categoria){
        if(categoria == Categoria.GOLD){
            return LIMITE_GOLD;
        }else if(categoria == Categoria.DIAMANTE){
            return LIMITE_DIAMANTE;
        }
        return LIMITE_BLACK;
    }
}
