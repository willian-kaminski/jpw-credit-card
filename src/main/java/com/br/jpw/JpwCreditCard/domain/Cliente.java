package com.br.jpw.JpwCreditCard.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.Embeddable;
import javax.validation.constraints.Email;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class Cliente {

    @NotNull
    private Long idCliente;

    @NotNull
    private String nome;

    @NotNull
    private String rg;

    @NotNull
    private String cpf;

    @NotNull
    private Double renda;

    @NotNull
    @Email
    private String email;

    @NotNull
    @JsonFormat(pattern="YYYY-MM-dd")
    private Date dataNascimento;

    @NotNull
    private String telefone;

}
