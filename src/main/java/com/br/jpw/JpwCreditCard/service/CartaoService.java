package com.br.jpw.JpwCreditCard.service;

import com.br.jpw.JpwCreditCard.domain.Cartao;
import com.br.jpw.JpwCreditCard.domain.CartaoExistDTO;
import com.br.jpw.JpwCreditCard.domain.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface CartaoService {

    ResponseEntity<Page<Cartao>> listarTodos(Pageable pageable);

    ResponseEntity<Cartao> listarPorId(Long id);

    ResponseEntity<Cartao> registrar(Cliente cliente);

    ResponseEntity<?> atualizar(Cartao cartao);

    ResponseEntity deletarPorId(Long id);

    ResponseEntity<CartaoExistDTO> existsByCpfAndIsValido(String cpf);

}
