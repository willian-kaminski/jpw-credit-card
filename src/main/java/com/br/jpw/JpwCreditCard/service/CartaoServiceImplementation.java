package com.br.jpw.JpwCreditCard.service;

import com.br.jpw.JpwCreditCard.domain.Cartao;
import com.br.jpw.JpwCreditCard.domain.CartaoExistDTO;
import com.br.jpw.JpwCreditCard.domain.Cliente;
import com.br.jpw.JpwCreditCard.repository.CartaoRepository;
import com.br.jpw.JpwCreditCard.util.RegrasCartao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CartaoServiceImplementation implements CartaoService{

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private RegrasCartao regrasCartao;

    @Override
    public ResponseEntity<Page<Cartao>> listarTodos(Pageable pageable) {
        return ResponseEntity.status(HttpStatus.OK).body(cartaoRepository.findAll(pageable));
    }

    @Override
    public ResponseEntity<Cartao> listarPorId(Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(cartaoRepository.findById(id).get());
    }

    @Transactional
    @Override
    public ResponseEntity<Cartao> registrar(Cliente cliente) {

        Cartao cartao = Cartao.builder()
                .cliente(cliente)
                .codigoCartao(regrasCartao.gerarCodigoCartao())
                .categoria(regrasCartao.gerarCategoria(cliente.getRenda()))
                .limite(regrasCartao.gerarLimite(regrasCartao.gerarCategoria(cliente.getRenda())))
                .dataValidade(regrasCartao.gerarDataValidade())
                .isAtivo(true)
                .build();

        System.out.println(cartao.toString());

        return ResponseEntity.status(HttpStatus.OK).body(cartaoRepository.save(cartao));
    }

    @Override
    public ResponseEntity<?> atualizar(Cartao cartao) {
        if(cartaoRepository.findById(cartao.getId()).isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(cartaoRepository.save(cartao));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                String.format("Resource not found for id %s", cartao.getId())
        );
    }

    @Override
    public ResponseEntity deletarPorId(Long id) {
        if(cartaoRepository.findById(id).isPresent()){
            cartaoRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                String.format("Resource not found for id %s", id)
        );
    }

    @Override
    public ResponseEntity<CartaoExistDTO> existsByCpfAndIsValido(String cpf) {
        if(cartaoRepository.findCartaoByCpf(cpf).isPresent()){
            return ResponseEntity.status(HttpStatus.OK).body(new CartaoExistDTO(cpf, Boolean.TRUE));
        }
        return ResponseEntity.status(HttpStatus.OK).body(new CartaoExistDTO(cpf, Boolean.FALSE));
    }
}
