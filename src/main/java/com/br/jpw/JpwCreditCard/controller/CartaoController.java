package com.br.jpw.JpwCreditCard.controller;

import com.br.jpw.JpwCreditCard.domain.Cartao;
import com.br.jpw.JpwCreditCard.domain.CartaoExistDTO;
import com.br.jpw.JpwCreditCard.domain.Cliente;
import com.br.jpw.JpwCreditCard.service.CartaoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/cartao")
@Api(tags = "cartao (Cartões)", description = "Localização: Cadastros >> Cartões")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @GetMapping
    @ApiOperation(value = "Retorna uma lista paginada de cartões")
    public ResponseEntity<Page<Cartao>> listarTodos(Pageable pageable){
        return cartaoService.listarTodos(pageable);
    }

    @GetMapping(path = "/{id}")
    @ApiOperation(value = "Retorna um cartão através do Id")
    public ResponseEntity<Cartao> listarTodos(@PathVariable Long id){
        return cartaoService.listarPorId(id);
    }

    @PostMapping
    @ApiOperation(value = "Registra um novo cartão na base de dados")
    public ResponseEntity<Cartao> register(@RequestBody @Valid Cliente cliente){
        return cartaoService.registrar(cliente);
    }

    @PutMapping
    @ApiOperation(value = "Atualiza o registro do cartão")
    public ResponseEntity<?> atualizar(@RequestBody @Valid Cartao cartao){
        return cartaoService.atualizar(cartao);
    }

    @DeleteMapping(path = "/{id}")
    @ApiOperation(value = "Remove um cartão através do Id")
    public ResponseEntity deletarPorId(@PathVariable Long id){
        return cartaoService.deletarPorId(id);
    }

    @GetMapping(path = "/cpf/{cpf}")
    @ApiOperation(value = "Verificar se o CPF se encontra cadastrado na base de dados")
    public ResponseEntity<CartaoExistDTO> verificarSeClienteJaEstaCadastrado(@PathVariable String cpf){
        return cartaoService.existsByCpfAndIsValido(cpf);
    }

}
