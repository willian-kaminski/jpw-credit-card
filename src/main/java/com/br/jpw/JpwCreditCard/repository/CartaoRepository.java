package com.br.jpw.JpwCreditCard.repository;

import com.br.jpw.JpwCreditCard.domain.Cartao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CartaoRepository extends PagingAndSortingRepository<Cartao, Long> {

    @Query(value = "select * from Cartao where cpf = ?1 and is_ativo = true", nativeQuery = true)
    Optional<Cartao> findCartaoByCpf(String cpf);

}
